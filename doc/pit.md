# PIT - Programmable Interval Timer

The PIT allows to:
- Trigger an interrupt on vector `0x20` (see **interrupts.md**) at a given frequency
- Use the PC Speaker

Its main usage is to allow task switching over processes.
