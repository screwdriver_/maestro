```
unsigned floor_log2(const unsigned n);
```

Returns the floor-ed value of the logarithm to base 2 of given **n**.
