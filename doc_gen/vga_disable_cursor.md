```
void vga_disable_cursor(void);
```

Disables the cursor for VGA text mode.
