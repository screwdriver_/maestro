```
# define MIN(a, b) ((a) <= (b) ? (a) : (b))
```

Returns the lowest value between **a** and **b**.
