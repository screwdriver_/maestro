```
void outw(uint16_t port, uint16_t value);
```

Writes two bytes to the specified port.
