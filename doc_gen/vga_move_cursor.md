```
void vga_move_cursor(vgapos_t x, vgapos_t y);
```

Moves the VGA text mode cursor to the specified position.
