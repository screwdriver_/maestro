```
extern int cpuid_available(void);
```

Tells whether `cpuid` is available or not.
