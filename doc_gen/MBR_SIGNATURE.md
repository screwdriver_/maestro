```
# define MBR_SIGNATURE 0x55aa
```

The signature of a Master Boot Record partition table (`0x55aa`).
