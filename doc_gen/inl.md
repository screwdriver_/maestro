```
uint32_t inl(uint16_t port);
```

Reads four bytes from the specified port.
