```
uint16_t inw(uint16_t port);
```

Reads two bytes from the specified port.
