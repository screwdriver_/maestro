```
extern void spin_unlock(spinlock_t *spinlock);
```

Unlocks the specified **spinlock**.
