```
extern void *cr2_get(void);
```

Returns the value of the CR2 register (address of memory access that caused the last page fault).
