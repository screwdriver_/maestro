```
struct regs
```

Structure containing registers, used to save a context.

Saved registers are: **ebp**, **esp**, **eip**, **eflags**, **eax**, **ebx**, **ecx**, **edx**, **esi**, **edi**
