```
extern void *cr3_get(void);
```

Returns the value of the CR3 register (current active page directory).
