```
extern void kernel_wait(void);
```

Pauses the current task until an interrupt is received.
