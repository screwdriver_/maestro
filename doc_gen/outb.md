```
void outb(uint16_t port, uint8_t value);
```

Writes one byte to the specified port.
