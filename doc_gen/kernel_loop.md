```
extern void kernel_loop(void);
```

Stops the current task and waits for an interruption. Will never return.
